import requests
import time

reddits = ['funny', 'AskReddit', 'todayilearned', 'soccer', 'worldnews']

def get_reddit_url(subreddit, limit=100):
    return "https://www.reddit.com/r/%s.json?sort=all&limit=%s" % (str(subreddit), str(limit))

def get_reddit(subreddit, show_output=False):
    start = time.time()
    url = get_reddit_url(str(subreddit))
    r = requests.get(url,  headers = {'User-agent': 'redit-agent 0.1'})
    j = r.json()
    json_data = j['data']['children']
    print("\n%s total results - %s" % (subreddit, len(json_data)))
    for i in json_data:
        title, link = i['data']['title'], i['data']['url']
        if show_output:
            print("#%s: %s (%s)" % (str(subreddit), title, link))
    print("DONE: %s\n" % subreddit)
    elapsed = time.time() - start
    return elapsed

def get_reddits():    
    return sum([get_reddit(r) for r in reddits])

start = time.time()
elapsed = get_reddits()
print('>>>  Cumulative:\t\t%s secs.' % elapsed)
print('>>>  Total:\t\t\t%s secs.' % str(time.time() - start))
