from concurrent.futures import ThreadPoolExecutor
from time import sleep
 
def return_after_3_secs(message):
    sleep(3)
    return message
 
pool = ThreadPoolExecutor(3)
 
future = pool.submit(return_after_3_secs, ("hello"))
print(future.done())
sleep(5)
print(future.done())
print(future.result())